#include <stdio.h>
#include <stdlib.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
  struct arvoreRB *pai;
} ArvoreRB;

ArvoreRB *criar_no(int valor) {
  ArvoreRB *novo_no = (ArvoreRB *)malloc(sizeof(ArvoreRB));
  novo_no->info = valor;
  novo_no->cor = RED;  // Novo nó sempre é vermelho
  novo_no->esq = NULL;
  novo_no->dir = NULL;
  novo_no->pai = NULL;
  return novo_no;
}

int eh_no_vermelho(ArvoreRB *no) {
  if (!no) return BLACK;
  return (no->cor == RED);
}

ArvoreRB *rotacao_esquerda(ArvoreRB *raiz, ArvoreRB *x) {
  ArvoreRB *y = x->dir;
  x->dir = y->esq;
  if (y->esq != NULL) y->esq->pai = x;
  y->pai = x->pai;
  if (x->pai == NULL) raiz = y;
  else if (x == x->pai->esq) x->pai->esq = y;
  else x->pai->dir = y;
  y->esq = x;
  x->pai = y;
  return raiz;
}

ArvoreRB *rotacao_direita(ArvoreRB *raiz, ArvoreRB *y) {
  ArvoreRB *x = y->esq;
  y->esq = x->dir;
  if (x->dir != NULL) x->dir->pai = y;
  x->pai = y->pai;
  if (y->pai == NULL) raiz = x;
  else if (y == y->pai->esq) y->pai->esq = x;
  else y->pai->dir = x;
  x->dir = y;
  y->pai = x;
  return raiz;
}

ArvoreRB *inserir_fixup(ArvoreRB *raiz, ArvoreRB *z) {
  while (eh_no_vermelho(z->pai)) {
    if (z->pai == z->pai->pai->esq) {
      ArvoreRB *y = z->pai->pai->dir;
      if (eh_no_vermelho(y)) {
        z->pai->cor = BLACK;
        y->cor = BLACK;
        z->pai->pai->cor = RED;
        z = z->pai->pai;
      } else {
        if (z == z->pai->dir) {
          z = z->pai;
          raiz = rotacao_esquerda(raiz, z);
        }
        z->pai->cor = BLACK;
        z->pai->pai->cor = RED;
        raiz = rotacao_direita(raiz, z->pai->pai);
      }
    } else {
      ArvoreRB *y = z->pai->pai->esq;
      if (eh_no_vermelho(y)) {
        z->pai->cor = BLACK;
        y->cor = BLACK;
        z->pai->pai->cor = RED;
        z = z->pai->pai;
      } else {
        if (z == z->pai->esq) {
          z = z->pai;
          raiz = rotacao_direita(raiz, z);
        }
        z->pai->cor = BLACK;
        z->pai->pai->cor = RED;
        raiz = rotacao_esquerda(raiz, z->pai->pai);
      }
    }
  }
  raiz->cor = BLACK;
  return raiz;
}

ArvoreRB *inserir(ArvoreRB *raiz, int valor) {
  ArvoreRB *z = criar_no(valor);
  ArvoreRB *y = NULL;
  ArvoreRB *x = raiz;

  while (x != NULL) {
    y = x;
    if (z->info < x->info) x = x->esq;
    else x = x->dir;
  }

  z->pai = y;
  if (y == NULL) raiz = z;
  else if (z->info < y->info) y->esq = z;
  else y->dir = z;

  return inserir_fixup(raiz, z);
}

ArvoreRB *encontrar_minimo(ArvoreRB *raiz) {
  while (raiz->esq != NULL) raiz = raiz->esq;
  return raiz;
}

ArvoreRB *encontrar_maximo(ArvoreRB *raiz) {
  while (raiz->dir != NULL) raiz = raiz->dir;
  return raiz;
}

ArvoreRB *transplantar(ArvoreRB *raiz, ArvoreRB *u, ArvoreRB *v) {
  if (u->pai == NULL) raiz = v;
  else if (u == u->pai->esq) u->pai->esq = v;
  else u->pai->dir = v;
  if (v != NULL) v->pai = u->pai;
  return raiz;
}

ArvoreRB *remover_fixup(ArvoreRB *raiz, ArvoreRB *x) {
  while (x != raiz && x->cor == BLACK) {
    if (x == x->pai->esq) {
      ArvoreRB *w = x->pai->dir;
      if (eh_no_vermelho(w)) {
        w->cor = BLACK;
        x->pai->cor = RED;
        raiz = rotacao_esquerda(raiz, x->pai);
        w = x->pai->dir;
      }
      if (eh_no_vermelho(w->esq) && eh_no_vermelho(w->dir)) {
        w->cor = RED;
        x = x->pai;
      } else {
        if (eh_no_vermelho(w->dir)) {
          w->esq->cor = BLACK;
          w->cor = RED;
          raiz = rotacao_direita(raiz, w);
          w = x->pai->dir;
        }
        w->cor = x->pai->cor;
        x->pai->cor = BLACK;
        w->dir->cor = BLACK;
        raiz = rotacao_esquerda(raiz, x->pai);
        x = raiz;
      }
    } else {
      ArvoreRB *w = x->pai->esq;
      if (eh_no_vermelho(w)) {
        w->cor = BLACK;
        x->pai->cor = RED;
        raiz = rotacao_direita(raiz, x->pai);
        w = x->pai->esq;
      }
      if (eh_no_vermelho(w->dir) && eh_no_vermelho(w->esq)) {
        w->cor = RED;
        x = x->pai;
      } else {
        if (eh_no_vermelho(w->esq)) {
          w->dir->cor = BLACK;
          w->cor = RED;
          raiz = rotacao_esquerda(raiz, w);
          w = x->pai->esq;
        }
        w->cor = x->pai->cor;
        x->pai->cor = BLACK;
        w->esq->cor = BLACK;
        raiz = rotacao_direita(raiz, x->pai);
        x = raiz;
      }
    }
  }
  x->cor = BLACK;
  return raiz;
}

ArvoreRB *remover(ArvoreRB *raiz, int valor) {
  ArvoreRB *z = raiz;
  ArvoreRB *x, *y;
  while (z != NULL) {
    if (valor < z->info) z = z->esq;
    else if (valor > z->info) z = z->dir;
    else break;
  }
  if (z == NULL) return raiz;
  y = z;
  int y_cor_original = y->cor;
  if (z->esq == NULL) {
    x = z->dir;
    raiz = transplantar(raiz, z, z->dir);
  } else if (z->dir == NULL) {
    x = z->esq;
    raiz = transplantar(raiz, z, z->esq);
  } else {
    y = encontrar_minimo(z->dir);
    y_cor_original = y->cor;
    x = y->dir;
    if (y->pai == z) x->pai = y;
    else {
      raiz = transplantar(raiz, y, y->dir);
      y->dir = z->dir;
      y->dir->pai = y;
    }
    raiz = transplantar(raiz, z, y);
    y->esq = z->esq;
    y->esq->pai = y;
    y->cor = z->cor;
  }
  if (y_cor_original == BLACK) raiz = remover_fixup(raiz, x);
  free(z);
  return raiz;
}

void in_order(ArvoreRB *a) {
  if (!a)
    return;
  in_order(a->esq);
  printf("%d ", a->info);
  in_order(a->dir);
}

void print(ArvoreRB *a, int spaces) {
  int i;
  for (i = 0; i < spaces; i++) printf(" ");
  if (!a) {
    printf("//\n");
    return;
  }

  printf("%d", a->info);
  if (a->cor == RED) printf(" (RED)\n");
  else printf(" (BLACK)\n");
  print(a->esq, spaces + 2);
  print(a->dir, spaces + 2);
}

int main() {
  ArvoreRB *raiz = NULL;
  raiz = inserir(raiz, 10);
  raiz = inserir(raiz, 20);
  raiz = inserir(raiz, 30);
  raiz = inserir(raiz, 15);
  raiz = inserir(raiz, 25);

  printf("Árvore Red-Black:\n");
  print(raiz, 0);

  raiz = remover(raiz, 20);

  printf("\nÁrvore Red-Black após remover o nó com valor 20:\n");
  print(raiz, 0);

  return 0;
}

